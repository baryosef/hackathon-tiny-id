
## Start
### change TIKA and CII-SDK URLs in `.env` file

### run `yarn start`


Runs the app in the development mode.\

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

---
---
---
### How to run TIKA server locally (for any reason)
### run tika locally on 'http://localhost:9998' 
### `export DOCKER_DEFAULT_PLATFORM=linux/amd64 && docker run -d -p 9998:9998 apache/tika`
