import { BigidTheme, BigidToastNotification } from '@bigid-ui/components';
import { ThemeProvider } from '@material-ui/core';
import React, { FC, useReducer } from 'react';
import './App.css';
import { AppContext, AppContextState, appReducer, getInitialReducerState } from './AppContext';
import { AppRoutes } from './AppRoutes';
import { Header } from './Header';
import { BigidPolicy2Icon } from '@bigid-ui/icons';

export const App: FC = () => {
  const [state, dispatch] = useReducer(appReducer, {}, getInitialReducerState);

  const contextValue: AppContextState = {
    state,
    dispatch,
  };

  console.log('state,,state,', state);

  return (
    <ThemeProvider theme={BigidTheme}>
      <AppContext.Provider value={contextValue}>
        <div className="app">
          <header className="app--header">
            <Header />
            <BigidToastNotification />
          </header>
          <main className="app--content">
            <AppRoutes />
          </main>
          {/* <footer><Subscribe></></footer> */}
        </div>
      </AppContext.Provider>
    </ThemeProvider>
  );
};
