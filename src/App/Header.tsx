import { BigidHeader } from '@bigid-ui/components';
import { BigidUserFilledIcon } from '@bigid-ui/icons';
import { noop } from 'lodash';
import React, { FC } from 'react';
import { ReactComponent as SmallIdLogo } from './SmallId.svg';

export const Header: FC = () => {
  return (
    <BigidHeader
      // logo={() => <BigidHorizontalLogoIcon height={24} />}
      logo={() => <SmallIdLogo />}
      accountInfo={{
        icon: BigidUserFilledIcon,
        accountName: 'Login',
        menu: [{ label: 'Welcome', onClick: noop }, { divider: true }],
      }}
    />
  );
};
