import { createContext, useContext } from 'react';
import { ClassifiersResultItem } from '../helpers/requests';

export interface AppContextState {
  state: AppReducerState;
  dispatch: React.Dispatch<AppReducerAction>;
}

export const AppContext = createContext<AppContextState>(null);

export function useAppContext() {
  const context = useContext(AppContext);
  if (!context) {
    console.error('useAppContext must be used within a AppContext.Provider');
  }
  return context;
}

export enum ReducerActions {
  'INIT',
  'UPDATE',
  'RESET',
}

export interface AppReducerState {
  isLoading: boolean;
  file: File;
  tikaResult: string;
  ciiResults: ClassifiersResultItem[];
  fetchDone: boolean;
}

export interface AppReducerAction {
  type: ReducerActions;
  payload: Partial<AppReducerState>;
}

export const appReducer: React.Reducer<AppReducerState, AppReducerAction> = (state, { type, payload }) => {
  switch (type) {
    case ReducerActions.UPDATE: {
      console.log('updating state', payload);
      return { ...state, ...payload };
    }

    case ReducerActions.INIT: {
      return getInitialReducerState(payload);
    }

    case ReducerActions.RESET: {
      return getInitialReducerState({});
    }

    default:
      return state;
  }
};

export function getInitialReducerState(overrides: Partial<AppReducerState>): AppReducerState {
  return {
    isLoading: false,
    file: null,
    tikaResult: null,
    ciiResults: null,
    fetchDone: false,
    ...overrides,
  };
}
