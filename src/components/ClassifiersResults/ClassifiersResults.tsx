import { BigidGridColumnTypes, BigidSimpleGrid, SimpleGridProps, SimpleGridRow } from '@bigid-ui/grid';
import { makeStyles } from '@material-ui/core';
import React, { FC, ReactText, useEffect, useMemo, useState } from 'react';
import { ClassifiersResultItem } from '../../helpers/requests';
import { BigidWarningOutlineIcon } from '@bigid-ui/icons';

export interface ClassifiersResultsProps {
  results: ClassifiersResultItem[];
}

const useStyles = makeStyles({
  root: {
    maxHeight: 400,
    overflow: 'hidden',
    display: 'flex',
    width: 800,
    flexDirection: 'column',
  },
  cellContentsWrapper: {
    wordBreak: 'break-word',
  },
  cellWithIcon: {
    display: 'flex',
    alignItems: 'center',
  },
  cellText: {
    marginLeft: 10,
  },
});

export const ClassifiersResults: FC<ClassifiersResultsProps> = ({ results }) => {
  const classes = useStyles({});
  const [rows, setRows] = useState<SimpleGridRow[]>([]);

  const getNameCellWithIcon = (classifierName: ReactText) => {
    return (
      <div className={classes.cellWithIcon}>
        <BigidWarningOutlineIcon />
        <div className={classes.cellText}>{classifierName}</div>
      </div>
    );
  };

  useEffect(() => {
    const newRows: SimpleGridRow[] = (results?.length ? results : []).map((item, i) => {
      return {
        id: i,
        ...item,
      };
    });
    setRows(newRows);
  }, [results]);

  const columns: SimpleGridProps['columns'] = useMemo(
    () => [
      {
        name: 'classifierName',
        title: 'Classifier',
        type: BigidGridColumnTypes.CUSTOM,
        getCellValue: row => getNameCellWithIcon(row.classifierName),
      },
      {
        name: 'matchedValue',
        title: 'Matched Value',
        type: BigidGridColumnTypes.TEXT,
        wordWrapEnabled: true,
        // eslint-disable-next-line react/display-name
        getCellValue: ({ matchedValue }: any) => {
          const value = matchedValue.trim(/[\s\n\t]+/, '');
          return value;
        },
      },
    ],
    [],
  );

  return results?.length ? (
    <div className={classes.root}>
      <BigidSimpleGrid showResizingControls={false} columns={columns} rows={rows}></BigidSimpleGrid>
    </div>
  ) : null;
};
