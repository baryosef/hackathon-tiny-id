import { BigidDialog, BigidTabsAndContent, BigidTabsAndContentProps } from '@bigid-ui/components';
import { BigidSearchQueryIcon } from '@bigid-ui/icons';
import { makeStyles } from '@material-ui/core';
import { noop } from 'lodash';
import React, { FC, useState } from 'react';

interface ResultsDebuggerProps {
  results: { title: string; value: string }[];
}

const useStyles = makeStyles({
  root: {
    position: 'fixed',
    bottom: 0,
    left: 0,
  },
});

export const ResultsDebugger: FC<ResultsDebuggerProps> = ({ results }) => {
  const classes = useStyles({});
  const [isOpen, setIsOpen] = useState(false);
  const [selectedIndex, setSelectedIndex] = useState(0);

  const handleOnClick = () => setIsOpen(true);
  const handleOnClose = () => setIsOpen(false);

  const hasResults = results?.some(res => res.value);

  const tabsAndContentConfig: BigidTabsAndContentProps = {
    tabProps: {
      tabs: (results || []).map(res => {
        return {
          label: res.title,
          data: {
            component: () => (
              <pre style={{ maxWidth: '400px', whiteSpace: 'pre-wrap' }}>
                <code>{res.value}</code>
              </pre>
            ),
          },
        };
      }),
      selectedIndex,
      onChange: tabIndex => {
        setSelectedIndex(tabIndex);
      },
    },
  };

  return (
    <div className={classes.root} onClickCapture={hasResults ? handleOnClick : noop}>
      <BigidSearchQueryIcon disabled={!hasResults} />
      <BigidDialog isOpen={hasResults && isOpen} onClose={handleOnClose} title={'Parsed text'}>
        <BigidTabsAndContent {...tabsAndContentConfig} />
      </BigidDialog>
    </div>
  );
};
