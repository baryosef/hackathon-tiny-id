import React, { FC } from 'react';
import './Loader.css';

export interface LoaderProps {
  isLoading: boolean;
}
export const Loader: FC<LoaderProps> = ({ isLoading }) => {
  return <div className={`box ${isLoading ? 'box--loading' : ''}`} />;
};
