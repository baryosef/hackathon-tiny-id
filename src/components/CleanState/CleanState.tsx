import React, { FC } from 'react';
import './CleanState.css';
import { BigidPolicy2Icon } from '@bigid-ui/icons';
import { BigidHeading3 } from '@bigid-ui/components';

export const CleanState: FC = () => {
  return (
    <div className="root">
      <div className="svgWrapper">
        <BigidPolicy2Icon size={30} />
      </div>
      <BigidHeading3>Your file is clean of private sensitive data</BigidHeading3>
    </div>
  );
};
