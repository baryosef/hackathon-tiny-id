import { BigidHeading1, SecondaryButton } from '@bigid-ui/components';
import { BigidCloudInfoBgPrIcon } from '@bigid-ui/icons';
import { makeStyles } from '@material-ui/core';
import React, { FC, useContext } from 'react';
import { AppContext, ReducerActions } from '../App/AppContext';
import { ClassifiersResults } from '../components/ClassifiersResults/ClassifiersResults';
import { ResultsDebugger } from '../components/ResultsDebugger/ResultsDebugger';
import { FileUploader } from './FileUploader';
import { ReactComponent as SmallIdBannerSvg } from './SmallIdBanner.svg';
import { ProductInfoContactUs } from '@bigid-ui/products-infos';
import { CleanState } from '../components/CleanState/CleanState';

const TRY_AGAIN_TEXT = 'Scan Another File';
const useStyles = makeStyles({
  root: {
    flex: '1 0 100%',
    display: 'flex',
    flexFlow: 'column nowrap',
    alignContent: 'center',
    justifyContent: 'flex-start',
    alignItems: 'center',
    padding: 40,
    paddingTop: 100,
    position: 'relative',
    gap: 30,
  },
  rootLoading: {
    padding: 10,
    transition: '1s all cubic-bezier(0.13, 0.64, 0.46, 1.2)',
  },
  backgroundLogo: {
    position: 'absolute',
    left: 0,
    top: 0,
  },
  bottomLogo: {
    position: 'absolute',
    right: 0,
    bottom: 0,
    transform: 'rotate(180deg)',
  },
  header: {
    gap: 20,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
    alignContent: 'center',
  },
  uploader: {
    minWidth: '50vw',
  },
  footerWrapper: {
    margin: 'auto 0 55px 0',
  },
});

export const Main: FC = () => {
  const classes = useStyles();
  const {
    dispatch,
    state: { isLoading, ciiResults, tikaResult, fetchDone },
  } = useContext(AppContext);

  return (
    <div className={classes.root + (isLoading || fetchDone ? ` ${classes.rootLoading}` : '')}>
      <span className={classes.backgroundLogo}>
        <BigidCloudInfoBgPrIcon />
      </span>
      <span className={classes.bottomLogo}>
        <BigidCloudInfoBgPrIcon />
      </span>
      <div className={classes.header}>
        <BigidHeading1>Upload & Discover</BigidHeading1>
        <SmallIdBannerSvg width="100%" height="100%" />
      </div>
      {fetchDone ? (
        ciiResults.length === 0 ? (
          <CleanState />
        ) : (
          <ClassifiersResults results={ciiResults} />
        )
      ) : (
        <div className={classes.uploader}>
          <FileUploader />
        </div>
      )}

      {fetchDone && (
        <SecondaryButton
          onClick={() => dispatch({ type: ReducerActions.RESET, payload: {} })}
          size={'large'}
          width="fill"
        >
          {TRY_AGAIN_TEXT}
        </SecondaryButton>
      )}
      <div className={classes.footerWrapper}>
        <ProductInfoContactUs
          title="Need the full solution?"
          text="SmallID provides the full discovery experience"
          buttonText="Start A Trial Now"
          onClick={() => window.open('https://www.smallid.com', '_blank')}
        />
      </div>
      <ResultsDebugger
        results={[
          { title: 'Tika Results', value: tikaResult },
          { title: 'Classifications Results', value: JSON.stringify(ciiResults, null, 2) },
        ]}
      />
    </div>
  );
};
