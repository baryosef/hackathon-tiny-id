import { BigidDropZone, BigidTooltip, PrimaryButton, toastNotificationService } from '@bigid-ui/components';
import React, { FC, useContext } from 'react';
import { ciiRequest, tikaRequest } from '../helpers/requests';
import { Loader } from '../components/Loader/Loader';
import { AppContext, ReducerActions } from '../App/AppContext';

const UPLOAD_BUTTON_TEXT = 'Scan File';

export const FileUploader: FC = () => {
  const {
    state: { isLoading, file },
    dispatch,
  } = useContext(AppContext);

  const handleOnDrop = async (files: File[]) => {
    const file = files.length ? files[0] : null;
    dispatch({ type: ReducerActions.UPDATE, payload: { file } });
  };

  console.log('file', file);

  const handleUploadFile = async () => {
    dispatch({ type: ReducerActions.UPDATE, payload: { isLoading: true } });
    try {
      const { data: tikaResult } = await tikaRequest(file);
      dispatch({ type: ReducerActions.UPDATE, payload: { tikaResult } });

      const ciiResults = await ciiRequest(tikaResult);
      dispatch({ type: ReducerActions.UPDATE, payload: { ciiResults, fetchDone: true } });
    } catch (e) {
      toastNotificationService.error('Error occurred', e);
      console.error(e);
    } finally {
      dispatch({ type: ReducerActions.UPDATE, payload: { isLoading: false } });
    }
  };

  console.log({ isLoading });
  return (
    <div style={{ textAlign: 'center' }}>
      <div>
        {file ? (
          <div style={{ display: 'flex', flexDirection: 'column', padding: 20 }}>
            <Loader isLoading={isLoading} />

            <BigidTooltip title={file.name}>
              <div
                style={{
                  textOverflow: 'ellipsis',
                  overflow: 'hidden',
                  whiteSpace: 'nowrap',
                  textAlign: 'center',
                  margin: '0 auto',
                  maxWidth: 400,
                }}
              >
                {file.name}
              </div>
            </BigidTooltip>
          </div>
        ) : (
          <BigidDropZone
            maxSize={100}
            multiple={false}
            files={file ? [file] : []}
            onDrop={handleOnDrop}
          ></BigidDropZone>
        )}
      </div>
      <div className="upload-container">
        <PrimaryButton
          color="purple"
          onClick={handleUploadFile}
          size={'large'}
          width="fill"
          disabled={!file || isLoading}
        >
          {UPLOAD_BUTTON_TEXT}
        </PrimaryButton>
      </div>
    </div>
  );
};
