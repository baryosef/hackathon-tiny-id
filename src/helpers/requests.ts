import axios from 'axios';

// will be proxied by the setupProxy.js file:
const TIKA_URL = `${process.env.REACT_APP_CORS_ANYWHERE_PREFIX}${process.env.REACT_APP_TIKA_SERVER}/tika/form`;
const CII_URL = `${process.env.REACT_APP_CORS_ANYWHERE_PREFIX}${process.env.REACT_APP_CLASIFICATIONS_SDK_SERVER}/staging/parse`;

export interface ClassifiersResultItem {
  matchedValue: string;
  classifierName: string;
}

export const tikaRequest = async (file: File) => {
  const form = new FormData();
  form.append('file', file);
  const res = await axios.post<string>(TIKA_URL, form, {
    headers: {
      'Content-Type': 'multipart/form-data',
      accept: 'text/plain',
    },
  });

  console.log('res tika', res);
  return res;
};

export const ciiRequest = async (data: any): Promise<ClassifiersResultItem[]> => {
  const res = await axios.post(
    CII_URL,
    { targetString: data },
    {
      headers: {
        accept: 'text/plain',
      },
    },
  );

  console.log('res sdk', res);

  return JSON.parse(res.data);
};
